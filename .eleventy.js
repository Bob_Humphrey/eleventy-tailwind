
// const dateMMMDYYYY = require('./plugins/date-MMMDYYYY.filter')

const syntaxHighlight = require("@11ty/eleventy-plugin-syntaxhighlight");

const sitemap = require("eleventy-plugin-sitemap");

module.exports = {
  markdownTemplateEngine: "njk"
};


module.exports = function (eleventyConfig) {

  eleventyConfig.setTemplateFormats([
    "md",
    "njk",
    "html",
    "css",
    "jpg",
    "png"
  ]);
  
  eleventyConfig.addPassthroughCopy("img/img-300");
  eleventyConfig.addPassthroughCopy("img/img-400");
  eleventyConfig.addPassthroughCopy("img/img-500");
  eleventyConfig.addPassthroughCopy("img/webp");
  eleventyConfig.addPassthroughCopy("img/webp-300");
  eleventyConfig.addPassthroughCopy("img/webp-400");
  eleventyConfig.addPassthroughCopy("img/webp-500");
  eleventyConfig.addPassthroughCopy("img/original");
 
  eleventyConfig.addPassthroughCopy('css');

  eleventyConfig.addPassthroughCopy("robots.txt");

  eleventyConfig.addPassthroughCopy("favicon.ico");

//   eleventyConfig.addPassthroughCopy('articles');

//   eleventyConfig.addPassthroughCopy('projects');

//   eleventyConfig.addPassthroughCopy('work');

//   eleventyConfig.addFilter("dateMMMDYYYY", dateMMMDYYYY);

  eleventyConfig.addShortcode("img", require("./plugins/img.shortcode"));

  eleventyConfig.addPlugin(syntaxHighlight);

  eleventyConfig.addPlugin(sitemap, {
    sitemap: {
      hostname: "https://flex.bob-humphrey.com",
    },
  });

  eleventyConfig.addCollection("componentsAlpha", function (collection) {
    return collection.getFilteredByGlob("components/*.md").sort(function (a, b) {
      let nameA = a.data.title.toUpperCase()
      let nameB = b.data.title.toUpperCase()
      if (nameA < nameB) return -1
      else if (nameA > nameB) return 1
      else return 0
    })
  });

  return {
    passthroughFileCopy: true
  }

}
