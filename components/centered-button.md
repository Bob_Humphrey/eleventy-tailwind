---
layout: component.njk
title: Centered Button
tags: component
---

<div class="flex items-center w-full bg-gray-200 h-32">
  <a class="bg-gray-400 hover:bg-gray-800 text-black hover:text-white text-center font-nunito_bold rounded w-48 py-2 mx-auto"
    href=""
  >
    Send
  </a>
</div>

```html
<div class="flex items-center w-full bg-gray-200 h-32">
  <a class="bg-gray-400 hover:bg-gray-800 text-black hover:text-white text-center font-nunito_bold rounded w-48 py-2 mx-auto"
    href=""
  >
    Send
  </a>
</div>
```