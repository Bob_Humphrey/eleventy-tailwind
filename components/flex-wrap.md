---
layout: component.njk
title: Flex Wrap
tags: component
---

<div class="w-full bg-gray-200 p-8">
  <div class="flex flex-wrap -mx-1">
    <div class="w-1/2 lg:w-1/4 px-1 mb-2">
      <div class="bg-gray-400 h-32"></div>
    </div>
    <div class="w-1/2 lg:w-1/4 px-1 mb-2">
      <div class="bg-gray-400 h-32"></div>
    </div>
    <div class="w-1/2 lg:w-1/4 px-1 mb-2">
      <div class="bg-gray-400 h-32"></div>
    </div>
    <div class="w-1/2 lg:w-1/4 px-1 mb-2">
      <div class="bg-gray-400 h-32"></div>
    </div>
    <div class="w-1/2 lg:w-1/4 px-1 mb-2">
      <div class="bg-gray-400 h-32"></div>
    </div>
    <div class="w-1/2 lg:w-1/4 px-1 mb-2">
      <div class="bg-gray-400 h-32"></div>
    </div>
    <div class="w-1/2 lg:w-1/4 px-1 mb-2">
      <div class="bg-gray-400 h-32"></div>
    </div>
    <div class="w-1/2 lg:w-1/4 px-1 mb-2">
      <div class="bg-gray-400 h-32"></div>
    </div>
  </div>
</div>

```html
<div class="w-full bg-gray-200 p-8">
  <div class="flex flex-wrap -mx-1">
    <div class="w-1/2 lg:w-1/4 px-1 mb-2">
      <div class="bg-gray-400 h-32"></div>
    </div>
    <div class="w-1/2 lg:w-1/4 px-1 mb-2">
      <div class="bg-gray-400 h-32"></div>
    </div>
    <div class="w-1/2 lg:w-1/4 px-1 mb-2">
      <div class="bg-gray-400 h-32"></div>
    </div>
    <div class="w-1/2 lg:w-1/4 px-1 mb-2">
      <div class="bg-gray-400 h-32"></div>
    </div>
    <div class="w-1/2 lg:w-1/4 px-1 mb-2">
      <div class="bg-gray-400 h-32"></div>
    </div>
    <div class="w-1/2 lg:w-1/4 px-1 mb-2">
      <div class="bg-gray-400 h-32"></div>
    </div>
    <div class="w-1/2 lg:w-1/4 px-1 mb-2">
      <div class="bg-gray-400 h-32"></div>
    </div>
    <div class="w-1/2 lg:w-1/4 px-1 mb-2">
      <div class="bg-gray-400 h-32"></div>
    </div>
  </div>
</div>
```