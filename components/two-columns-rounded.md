---
layout: component.njk
title: Two Columns with Rounded Corners
tags: component
---

<div class="grid grid-cols-2 py-4 h-48">
  <div class="bg-gray-200 rounded-l-md">
  </div>
  <div class="bg-gray-300 rounded-r-md">
  </div>
</div>

```html
<div class="grid grid-cols-2 py-4 h-48">
  <div class="bg-gray-200 rounded-l-md">
  </div>
  <div class="bg-gray-300 rounded-r-md">
  </div>
</div>
```