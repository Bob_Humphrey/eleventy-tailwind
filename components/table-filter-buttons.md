---
layout: component.njk
title: Table Filter Buttons
tags: component
---

<div class="flex w-full font-nunito_light justify-center bg-gray-200 pt-6 pb-4">
  <div class="bg-gray-400 text-sm text-center rounded-full cursor-pointer px-2 mx-2 mb-2">
    All
  </div>
  <div class="bg-gray-700 text-white text-sm text-center rounded-full cursor-pointer px-2 mx-2 mb-2">
    Open
  </div>
  <div class="bg-gray-400 text-sm text-center rounded-full cursor-pointer px-2 mx-2 mb-2">
    Closed
  </div>
</div>

```html
<div class="flex w-full font-nunito_light justify-center bg-gray-200 pt-6 pb-4">
  <div class="bg-gray-400 text-sm text-center rounded-full cursor-pointer px-2 mx-2 mb-2">
    All
  </div>
  <div class="bg-gray-700 text-white text-sm text-center rounded-full cursor-pointer px-2 mx-2 mb-2">
    Open
  </div>
  <div class="bg-gray-400 text-sm text-center rounded-full cursor-pointer px-2 mx-2 mb-2">
    Closed
  </div>
</div>
```