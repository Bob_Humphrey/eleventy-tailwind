---
layout: component.njk
title: Centered Columns
tags: component
---

<div class="flex w-full justify-center bg-gray-200 py-6">
  <div class="w-2/3 grid lg:grid-cols-3 h-64">
    <div class="bg-gray-400"></div>
    <div class="bg-gray-300"></div>
    <div class="bg-gray-400"></div>
  </div>
</div>

```html
<div class="flex w-full justify-center bg-gray-200 py-6">
  <div class="w-2/3 grid lg:grid-cols-3 h-64">
    <div class="bg-gray-400"></div>
    <div class="bg-gray-300"></div>
    <div class="bg-gray-400"></div>
  </div>
</div>
```