---
layout: component.njk
title: Previous/Next Buttons
tags: component
---

<div class="flex flex-col lg:flex-row items-center lg:justify-between w-full bg-gray-200 font-nunito_bold py-10 lg:px-16">
  <a
  class="lg:justify-start bg-gray-400 hover:bg-gray-800 text-gray-700 hover:text-white text-center rounded py-2 mx-2 mb-2 lg:mb-0 w-48 "
  href="/"
  >
    Previous
  </a>
  <a
  class="lg:justify-end bg-gray-400 hover:bg-gray-800 text-gray-700 hover:text-white text-center rounded py-2 mx-2 mb-2 lg:mb-0 w-48 "
  href="/"
  >
    Next
  </a>
</div>

```html
<div class="flex flex-col lg:flex-row items-center lg:justify-between w-full bg-gray-200 font-nunito_bold py-10 lg:px-16">
  <a
  class="lg:justify-start bg-gray-400 hover:bg-gray-800 text-gray-700 hover:text-white text-center rounded py-2 mx-2 mb-2 lg:mb-0 w-48 "
  href="/"
  >
    Previous
  </a>
  <a
  class="lg:justify-end bg-gray-400 hover:bg-gray-800 text-gray-700 hover:text-white text-center rounded py-2 mx-2 mb-2 lg:mb-0 w-48 "
  href="/"
  >
    Next
  </a>
</div>
```