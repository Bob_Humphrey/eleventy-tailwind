---
layout: component.njk
title: Score
tags: component
---

<div class=" flex w-full lg:w-1/3 justify-center bg-gray-200 py-6 mx-auto">
  <div class="bg-gray-400 h-4 w-4 mx-3 rounded-lg">
    &nbsp;
  </div>
  <div class="bg-gray-400 h-4 w-4 mx-3 rounded-lg">
    &nbsp;
  </div>
  <div class="bg-gray-800 h-4 w-4 mx-3 rounded-lg">
    &nbsp;
  </div>
  <div class="bg-gray-400 h-4 w-4 mx-3 rounded-lg">
    &nbsp;
  </div>
  <div class="bg-gray-400 h-4 w-4 mx-3 rounded-lg">
    &nbsp;
  </div>
</div>

```html
<div class=" flex w-full lg:w-1/3 justify-center bg-gray-200 py-6 mx-auto">
  <div class="bg-gray-400 h-4 w-4 mx-3 rounded-lg">
    &nbsp;
  </div>
  <div class="bg-gray-400 h-4 w-4 mx-3 rounded-lg">
    &nbsp;
  </div>
  <div class="bg-gray-800 h-4 w-4 mx-3 rounded-lg">
    &nbsp;
  </div>
  <div class="bg-gray-400 h-4 w-4 mx-3 rounded-lg">
    &nbsp;
  </div>
  <div class="bg-gray-400 h-4 w-4 mx-3 rounded-lg">
    &nbsp;
  </div>
</div>
```