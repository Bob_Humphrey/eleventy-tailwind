---
layout: component.njk
title: Centered Column
tags: component
---

<div class="flex w-full justify-center bg-gray-200 py-6">
  <div class="w-2/3 bg-gray-400 h-64"></div>
</div>

```html
<div class="flex w-full justify-center bg-gray-200 py-6">
  <div class="w-2/3 bg-gray-400 h-64"></div>
</div>
```