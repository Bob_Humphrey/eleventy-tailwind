---
layout: component.njk
title: Centered Horizontal List
tags: component
---

<div class="flex justify-center flex-col lg:flex-row w-full bg-gray-200 
font-nunito_regular text-gray-700 py-10 px-8 mb-6">
    <a class="px-4" href="/">
        Experience
    </a>
    <a class="px-4" href="/">
        Projects
    </a>
    <a class="px-4" href="/">
        Skills
    </a>
</div>

```html
<div class="flex justify-center flex-col lg:flex-row w-full bg-gray-200 
font-nunito_regular text-gray-700 py-10 px-8 mb-6">
    <a class="px-4" href="/">
        Experience
    </a>
    <a class="px-4" href="/">
        Projects
    </a>
    <a class="px-4" href="/">
        Skills
    </a>
</div>
```