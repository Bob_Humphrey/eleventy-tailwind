---
layout: component.njk
title: Cards with Equal Height
tags: component
---

<div class="w-full sm:flex flex-wrap -mx-2">

  <div class="flex items-stretch sm:w-1/2 md:w-1/3 px-2 mb-4">
    <div class="flex flex-col bg-gray-100 rounded-lg overflow-hidden">
      <img class="w-full" src="/img/originals/misc/001.jpg" 
          loading="lazy" 
          alt="Spartus camera"
        >
      <div class="px-6 py-4">
        <div class="text-lg font-nunito_bold">Spartus </div>
        <div class="text-sm font-nunito_light">
          Voluptatem eum nobis blanditiis eos laudantium cumque aliquid. 
          Quibusdam incidunt architecto vel sunt alias. Reiciendis quisquam 
          atque magnam. Quam aut ea sequi. 
        </div>
        <div class="font-nunito_bold">$77.50</div>
      </div>
    </div>
  </div>

  <div class="flex items-stretch sm:w-1/2 md:w-1/3 px-2 mb-4">
    <div class="flex flex-col bg-gray-100 rounded-lg overflow-hidden">
      <img class="w-full" src="/img/originals/misc/002.jpg" 
          loading="lazy" 
          alt="Argus Seventy-Five camera"
        >
      <div class="px-6 py-4">
        <div class="text-lg font-nunito_bold">Argus Seventy-Five</div>
        <div class="text-sm font-nunito_light">
          Sunt autem quia ea quam tempora perspiciatis voluptate dolorem. 
          Repellendus est fuga molestiae modi.
        </div>
        <div class="font-nunito_bold">$39.99</div>
      </div>
    </div>
  </div>

  <div class="flex items-stretch sm:w-1/2 md:w-1/3 px-2 mb-4">
    <div class="flex flex-col bg-gray-100 rounded-lg overflow-hidden">
      <img class="w-full" src="/img/originals/misc/003.jpg" 
          loading="lazy" 
          alt="Kodak Instamatic 100 camera"
        >
      <div class="px-6 py-4">
        <div class="text-lg font-nunito_bold">Kodak Instamatic 100</div>
        <div class="text-sm font-nunito_light">
          Pariatur perspiciatis doloribus deserunt sed eligendi veritatis 
          corrupti. Inventore doloremque quia doloribus aliquid. Qui earum 
          rerum eius ad qui.
        </div>
        <div class="font-nunito_bold">$23.25</div>
      </div>
    </div>
  </div>

</div>

```html
<div class="w-full sm:flex flex-wrap -mx-2">

  <div class="flex items-stretch sm:w-1/2 md:w-1/3 px-2 mb-4">
    <div class="flex flex-col bg-gray-100 rounded-lg overflow-hidden">
      <img class="w-full" src="/img/originals/misc/001.jpg" 
          loading="lazy" 
          alt="Spartus camera"
        >
      <div class="px-6 py-4">
        <div class="text-lg font-nunito_bold">Spartus </div>
        <div class="text-sm font-nunito_light">
          Voluptatem eum nobis blanditiis eos laudantium cumque aliquid. 
          Quibusdam incidunt architecto vel sunt alias. Reiciendis quisquam 
          atque magnam. Quam aut ea sequi. 
        </div>
        <div class="font-nunito_bold">$77.50</div>
      </div>
    </div>
  </div>

  <div class="flex items-stretch sm:w-1/2 md:w-1/3 px-2 mb-4">
    <div class="flex flex-col bg-gray-100 rounded-lg overflow-hidden">
      <img class="w-full" src="/img/originals/misc/002.jpg" 
          loading="lazy" 
          alt="Argus Seventy-Five camera"
        >
      <div class="px-6 py-4">
        <div class="text-lg font-nunito_bold">Argus Seventy-Five</div>
        <div class="text-sm font-nunito_light">
          Sunt autem quia ea quam tempora perspiciatis voluptate dolorem. 
          Repellendus est fuga molestiae modi.
        </div>
        <div class="font-nunito_bold">$39.99</div>
      </div>
    </div>
  </div>

  <div class="flex items-stretch sm:w-1/2 md:w-1/3 px-2 mb-4">
    <div class="flex flex-col bg-gray-100 rounded-lg overflow-hidden">
      <img class="w-full" src="/img/originals/misc/003.jpg" 
          loading="lazy" 
          alt="Kodak Instamatic 100 camera"
        >
      <div class="px-6 py-4">
        <div class="text-lg font-nunito_bold">Kodak Instamatic 100</div>
        <div class="text-sm font-nunito_light">
          Pariatur perspiciatis doloribus deserunt sed eligendi veritatis 
          corrupti. Inventore doloremque quia doloribus aliquid. Qui earum 
          rerum eius ad qui.
        </div>
        <div class="font-nunito_bold">$23.25</div>
      </div>
    </div>
  </div>

</div>
```