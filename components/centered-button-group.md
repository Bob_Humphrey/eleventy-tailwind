---
layout: component.njk
title: Centered Button Group
tags: component
---

<div class="flex flex-col lg:flex-row items-center justify-center w-full bg-gray-200 font-nunito_bold h-48">
  <a
    class="bg-gray-400 hover:bg-gray-800 text-gray-700 hover:text-white text-center rounded py-2 mx-2 mb-2 lg:mb-0 w-48"
    href="/"
  >
    Send
  </a>
  <a
    class="bg-gray-400 hover:bg-gray-800 text-gray-700 hover:text-white text-center rounded py-2 mx-2 mb-2 lg:mb-0 w-48"
    href="/"
  >
    Cancel
  </a>
</div>

```html
<div class="flex flex-col lg:flex-row items-center justify-center w-full bg-gray-200 font-nunito_bold h-48">
  <a
    class="bg-gray-400 hover:bg-gray-800 text-gray-700 hover:text-white text-center rounded py-2 mx-2 mb-2 lg:mb-0 w-48"
    href="/"
  >
    Send
  </a>
  <a
    class="bg-gray-400 hover:bg-gray-800 text-gray-700 hover:text-white text-center rounded py-2 mx-2 mb-2 lg:mb-0 w-48"
    href="/"
  >
    Cancel
  </a>
</div>
```