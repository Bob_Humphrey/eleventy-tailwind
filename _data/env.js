const environment = process.env.ELEVENTY_ENV
const PROD_ENV = 'production'
const prodUrl = 'https://flex.bob-humphrey.com'
const devUrl = 'http://localhost:8080'
const baseUrl = environment === PROD_ENV ? prodUrl : devUrl
const isProd = environment === PROD_ENV
const currentYear = new Date().getFullYear()

module.exports = {
  title: 'Tailwind CSS Examples',
  author: 'Bob Humphrey',
  description: 'A collection of website components that have been styled with '
    + 'Tailwind CSS and can be used as examples when creating new '
    + 'websites.',
  environment,
  isProd,
  baseUrl,
  currentYear
}